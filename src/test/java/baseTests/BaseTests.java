package baseTests;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import pageObjects.EnterInsurantDataPage;
import pageObjects.EnterProductDataPage;
import pageObjects.EnterVehicleDataPage;
import pageObjects.SelectPriceOptionPage;
import pageObjects.SendQuotePage;
import utils.Utils;
import utils.Web;

public class BaseTests {
	
	public EnterVehicleDataPage add = new EnterVehicleDataPage();
	public EnterInsurantDataPage adddp = new EnterInsurantDataPage();
	public EnterProductDataPage addds = new EnterProductDataPage();
	public SelectPriceOptionPage addp = new SelectPriceOptionPage();
	public SendQuotePage addqp = new SendQuotePage();

    public void setup(){
    	Web.getCurrentDriver();
    	Web.loadPage(Utils.getBaseUrl());
        Web.getCurrentDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void tearDown(){Web.close();}
    
}
