package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class Web {

    public static WebDriverWait wait;
    public static WebDriver navegador;

    public static WebDriver getCurrentDriver() {

        if(navegador == null) {

            System.setProperty("webdriver.chrome.driver", "src/Driver/chromedriver.exe");
            navegador = new ChromeDriver();
            wait = new WebDriverWait(navegador, 10);
            navegador.manage().window().maximize();
            navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

        return navegador;
    }
    public static void close(){
        getCurrentDriver().quit();
        navegador = null;
    }
    public static void loadPage(String url){
        getCurrentDriver().get(url);
    }
}