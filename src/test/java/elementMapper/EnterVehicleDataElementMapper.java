package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EnterVehicleDataElementMapper {

    @FindBy(css = "#entervehicledata > span")
    public WebElement contador;

    @FindBy(id = "make")
    public WebElement make;

    @FindBy(id = "model")
    public WebElement model;

    @FindBy(id = "cylindercapacity")
    public WebElement cylindercapacity;

    @FindBy(id = "engineperformance")
    public WebElement engineperformance;

    @FindBy(id = "dateofmanufacture")
    public WebElement dateofmanufacture;

    @FindBy(id = "numberofseats")
    public WebElement numberofseats;

    @FindBy(css = "#insurance-form > div > section:nth-child(1) > div:nth-child(7) > p > label:nth-child(1) > span")
    public WebElement righthanddriveyes;

    @FindBy(id = "numberofseatsmotorcycle")
    public WebElement numberofseatsmotorcycle;

    @FindBy(id = "fuel")
    public WebElement fuel;

    @FindBy(id = "payload")
    public WebElement payload;

    @FindBy(id = "totalweight")
    public WebElement totalweight;

    @FindBy(id = "listprice")
    public WebElement listprice;

    @FindBy(id = "licenseplatenumber")
    public WebElement licenseplatenumber;

    @FindBy(id = "annualmileage")
    public WebElement annualmileage;

    @FindBy(id = "nextenterinsurantdata")
    public WebElement btnnextenterinsurantdata;



}
