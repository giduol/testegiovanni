package pageObjects;

import elementMapper.SendQuoteElementMapper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Web;

import static utils.Web.navegador;

public class SendQuotePage extends SendQuoteElementMapper {

    public SendQuotePage() {PageFactory.initElements(Web.getCurrentDriver(), this);}

    public void inseriremail(String mail) {email.sendKeys(mail);}

    public void inserirusername(String nome) {username.sendKeys(nome);}

    public void inserirsenha(String senha) {password.sendKeys(senha);}

    public void confimarsenha(String senha) {confirmpassword.sendKeys(senha);}

    public void clicarbtnenvio() {btnsendemail.click();}

    public String pegarmsgconfirmacao() {
        WebDriverWait wait = new WebDriverWait(navegador, 5);
        WebElement msgsucessconfirm = wait.until(ExpectedConditions.visibilityOf(msgsucess));
        return msgsucessconfirm.getText();}

    public void clicarbtnconfirm() {btnconfirm.click();}
}
