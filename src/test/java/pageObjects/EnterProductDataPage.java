package pageObjects;

import elementMapper.EnterProductDataElementMapper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Web;

import static utils.Web.navegador;

public class EnterProductDataPage extends EnterProductDataElementMapper {

    public EnterProductDataPage() {
        PageFactory.initElements(Web.getCurrentDriver(), this);}

    public String validarcontadorproductpage() {
        WebDriverWait wait = new WebDriverWait(navegador, 5);
        WebElement contadorpage = wait.until(ExpectedConditions.visibilityOf(contador));
        return contadorpage.getText();}

    public void incluirdatainicio(String datainicio) {startdate.sendKeys(datainicio);}
        public void selecionarvalorseguro(String valor) {insurancesum.sendKeys(valor);}
        public void selecionarclassificacao(String classif) {meritrating.sendKeys(classif);}
        public void selecionaropcapdanos(String danos) {damageinsurance.sendKeys(danos);}
        public void incluirprodutoopcional() {optional.click();}
        public void escolhercarrocortesia(String carrocort) {courtesycar.sendKeys(carrocort);}
        public void clicarbtnnextprice() {btnnextprice.click();}
}
