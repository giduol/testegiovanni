package pageObjects;

import elementMapper.SelectPriceOptionElementMapper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Web;

import static utils.Web.navegador;

public class SelectPriceOptionPage extends SelectPriceOptionElementMapper {

    public SelectPriceOptionPage() {
        PageFactory.initElements(Web.getCurrentDriver(), this);}

    public String validarcontadorpricepage() {
        WebDriverWait wait = new WebDriverWait(navegador, 5);
        WebElement contadorpage = wait.until(ExpectedConditions.visibilityOf(contador));
        return contadorpage.getText();}

    public void escolherplano() {ultimate.click();}
    public void clicarbtnnextquote() {btnnextquote.click();}
}
